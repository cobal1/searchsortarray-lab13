**SEARCH**
https://www.mainframebug.com/tuts/COBOL/module9/Top9_COBOL_SEARCH_Sequential_search.php
https://www.ibmmainframer.com/cobol-tutorial/cobol-search-statement/
**SEARCH ALL**
https://www.mainframebug.com/tuts/COBOL/module9/Top10_COBOL_SEARCH_ALL_Binary_search.php
**SORT**
https://www.ibm.com/docs/en/cobol-zos/6.2?topic=tables-sorting-table
https://www.mainframebug.com/tuts/COBOL/module12/Top1_SORT_in_COBOL.php

1. Load File To Array
2. Search Array
3. Sort Array
4. Binary Search Array
5. Save Sorted Array To File